//
//  GIFDetailsInteractorTests.swift
//  MobimeoChallengeTests
//
//  Created by Marwan Ayman on 13/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import XCTest
@testable import MobimeoChallenge

class GIFDetailsInteractorTests: XCTestCase {

    func testInteractorData() {
        let detailsInteractor = GIFDetailsInteractor()
        XCTAssertNotNil(detailsInteractor)
        let original  = OriginalGIF(url: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")
        let downSized = OriginalGIF(url: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")

        let images = Images(original: original, downSizedImage: downSized)
        let giphy = Giphy(images: images, title: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif", rating: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")
        let donationListPresenter = GIFDetailsPresenter.init(interface: GIFDetailsViewController(), interactor: detailsInteractor, router: GIFDetailsRouter(), gifImage: giphy)
        detailsInteractor.presenter = donationListPresenter
        XCTAssertNotNil(detailsInteractor.presenter?.getImage())
    }
}
