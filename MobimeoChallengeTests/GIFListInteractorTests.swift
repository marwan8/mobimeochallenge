//
//  GIFListInteractorTests.swift
//  MobimeoChallengeTests
//
//  Created by Marwan Ayman on 13/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import XCTest
@testable import MobimeoChallenge

class GIFListInteractorTests: XCTestCase {
    func testInteractorData() {
        let listInteractor = MockInteractor()
        listInteractor.loadTrendingGIFs()
        XCTAssertNotNil(listInteractor)

        let listPresenter = MockPresenter()
        listInteractor.presenter = listPresenter
        XCTAssertNotNil(listInteractor.presenter)
    }

    func testInteractorLoadingData() {
        let exp = expectation(description: "Test after 5 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 5.0)
        if result == XCTWaiter.Result.timedOut {

            let listInteractor = GIFListInteractor(dataProvider: GIFTrendingListDataProvider.shared)
            listInteractor.loadTrendingGIFs()
            XCTAssertNotNil(listInteractor)

            let listPresenter = TestPresenter(interface: GIFListViewController(), interactor: listInteractor, router: GIFTrendingListRouter())
            listInteractor.presenter = listPresenter
            XCTAssertNotNil(listInteractor.presenter)

            if listPresenter.isItemsListLoaded {
                XCTAssertGreaterThan(listPresenter.itemCount, 0)
                XCTAssertNotNil(listPresenter.gifList?[0])
            } else if listPresenter.isLoadingError {
                XCTAssertEqual(listPresenter.itemCount, 0)
                XCTAssertNil(listPresenter.gifList?[0])
            }
        }
    }

    func testInteractorErrorLoading() {
        let gifListInteractor = GIFListInteractor(dataProvider: ErrorDataProvider.shared)
        gifListInteractor.loadTrendingGIFs()

        let gifListPresenter = TestPresenter(interface: GIFListViewController(), interactor: gifListInteractor, router: GIFTrendingListRouter())
        gifListInteractor.presenter = gifListPresenter
        XCTAssertNotNil(gifListInteractor.presenter)
        XCTAssertNotNil(gifListInteractor)
        if gifListPresenter.isItemsListLoaded {
            XCTAssertGreaterThan(gifListPresenter.itemCount, 0)
            XCTAssertNotNil(gifListPresenter.gifList?[0])
        } else if gifListPresenter.isLoadingError {
            XCTAssertEqual(gifListPresenter.itemCount, 0)
            XCTAssertNil(gifListPresenter.gifList?[0])
        }
    }

    func testEmptyLoading() {
        let gifListInteractor = EmptyInteractor()
        let gifListPresenter = TestPresenter(interface: GIFListViewController(), interactor: gifListInteractor, router: GIFTrendingListRouter())
        gifListInteractor.presenter = gifListPresenter

        gifListInteractor.loadTrendingGIFs()
        XCTAssertNotNil(gifListInteractor)
        XCTAssertNotNil(gifListInteractor.presenter)
        XCTAssertEqual(gifListPresenter.itemCount, 0)
    }

    func testErrorLoading() {
        let gifListInteractor = ErrorInteractor()
        let gifListPresenter = TestPresenter(interface: GIFListViewController(), interactor: gifListInteractor, router: GIFTrendingListRouter())
        gifListInteractor.presenter = gifListPresenter

        gifListInteractor.loadTrendingGIFs()
        XCTAssertNotNil(gifListInteractor)
        XCTAssertNotNil(gifListInteractor.presenter)
        XCTAssertEqual(gifListPresenter.itemCount, 0)

        gifListPresenter.showDetails(of: 0)
    }

    func testShowDetails() {
        let listInteractor = MockInteractor()
        let router = ListMockRouter()
        router.viewController = ListMockRouter.createMockModule()
        let listPresenter = GIFListPresenter(interface: GIFListViewController(), interactor: listInteractor, router: router)
        listInteractor.presenter = listPresenter

        listInteractor.loadTrendingGIFs()
        XCTAssertNotNil(listInteractor)
        XCTAssertNotNil(listInteractor.presenter)
        listPresenter.showDetails(of: 0)
    }

}

class ListMockRouter: GIFTrendingListRouter {
    static func createMockModule() -> UIViewController {
        let view = GIFListViewController(nibName: nil, bundle: nil)
        let interactor = MockInteractor()
        let router = GIFTrendingListRouter()
        let presenter = GIFListPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }

    override func showDetails(of image: Giphy) {
        let viewController = GIFListViewController()
        let detailsViewController = GIFDetailsRouter.createDetailsModule(of: image)
        viewController.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

class ErrorDataProvider: GIFTrendingListDataProvider {

    override func loadItemList(completion: @escaping RequestCompletion) {
        let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data do not exist"])
        completion(nil, error)
    }
}

class EmptyInteractor: GIFListInteractorProtocol {
    var presenter: GIFListPresenterProtocol?
    func loadTrendingGIFs() {
        presenter?.refreshListWithGIFs(gifList: [])
    }
}

class ErrorInteractor: GIFListInteractorProtocol {
    var presenter: GIFListPresenterProtocol?
    func loadTrendingGIFs() {
        let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Data do not exist"])
        presenter?.handleError(error: error)
    }
}

class EmptyDataProvider: GIFTrendingListDataProvider {

    override func loadItemList(completion: @escaping RequestCompletion) {
        let itemList = GiphyList(data: [])
        completion(itemList, nil)
    }
}

class TestPresenter: GIFListPresenter {
    var isItemsListLoaded = false
    var isLoadingError = false

    override func refreshListWithGIFs(gifList: [Giphy]) {
        super.refreshListWithGIFs(gifList: gifList)
        isItemsListLoaded = true
    }

    override func handleError(error: Error) {
        super.handleError(error: error)
        isLoadingError = true
    }
}
