//
//  MobimeoChallengeTests.swift
//  MobimeoChallengeTests
//
//  Created by Marwan Ayman on 03/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import XCTest
@testable import MobimeoChallenge

class MobimeoChallengeTests: XCTestCase {

    func testMockedData() {
        let testPresenter = MockPresenter()
        testPresenter.loadTrendingGIFs()
        let count = testPresenter.itemCount
        XCTAssertEqual(count, 2)

        let firstItem = testPresenter.item(at: 0)
        XCTAssertNotNil(firstItem)

        let secItem = testPresenter.item(at: 1)
        XCTAssertNotNil(secItem)

        let thirItem = testPresenter.item(at: 2)
        XCTAssertNil(thirItem)
    }
}

class MockDataProvider: GIFTrendingListDataProvider {

    override func loadItemList(completion: @escaping RequestCompletion) {
        let original  = OriginalGIF(url: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")
        let downSized = OriginalGIF(url: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")
        let images = Images(original: original, downSizedImage: downSized)
        let giphy = Giphy(images: images, title: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif", rating: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")
        let gifList = GiphyList(data: [giphy, giphy])
        completion(gifList, nil)
    }
}

class MockInteractor: GIFListInteractorProtocol {
    var presenter: GIFListPresenterProtocol?
    let dataProvider = MockDataProvider()
    func loadTrendingGIFs() {
        dataProvider.loadItemList { [weak self] (result, _) in
            if let itemList = result as? GiphyList {
                self?.presenter?.refreshListWithGIFs(gifList: itemList.data ?? [])
            }
        }
    }
}

class MockPresenter: GIFListPresenterProtocol {

    private(set) var gifList: [Giphy]?

    var isUserIsAuthenticatedCalled = false
    var isUserNeedsToAuthenticateCalled = false

    func item(at index: Int) -> Giphy? {
        guard let count = gifList?.count, index < count  else {
            return nil
        }
        return gifList?[index]
    }

    func loadTrendingGIFs() {
        let original  = OriginalGIF(url: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")
        let downSized = OriginalGIF(url: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")

        let images = Images(original: original, downSizedImage: downSized)

        let giphy = Giphy(images: images, title: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif", rating: "https://media3.giphy.com/media/j2MDHOKkF1Ys5NsOff/giphy.gif?cid=60f6040ca5e33544c01f5c582fc6b01ba3e03c106427938f&rid=giphy.gif")
        self.gifList = [giphy, giphy]
    }

    func refreshListWithGIFs(gifList: [Giphy]) {
        self.gifList = gifList
    }
    
    func showDetails(of index: Int) {

    }

    var itemCount: Int {
        return gifList?.count ?? 0
    }

    func handleError(error: Error) {

    }
}
