//
//  GIFDetailsViewController.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 10/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

class GIFDetailsViewController: UIViewController, GIFDetailsViewProtocol {
    var presenter: GIFDetailsPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = &&"details"
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        imageView.contentMode = .scaleAspectFit
        view.backgroundColor = .white
        view.addSubview(imageView)

        imageView.translatesAutoresizingMaskIntoConstraints = true
        imageView.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        imageView.autoresizingMask = [UIView.AutoresizingMask.flexibleLeftMargin, UIView.AutoresizingMask.flexibleRightMargin, UIView.AutoresizingMask.flexibleTopMargin, UIView.AutoresizingMask.flexibleBottomMargin]

        if let gifImage = presenter?.getImage() {
            imageView.sd_setImage(with: URL(string: gifImage.images?.original?.url ?? ""))
        }
    }
}
