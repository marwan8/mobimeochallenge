//
//  GIFDetailsPresenter.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 10/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

class GIFDetailsPresenter: GIFDetailsPresenterProtocol {
    weak private var view: GIFDetailsViewProtocol?
    var interactor: GIFDetailsInteractorProtocol?
    private let router: GIFDetailsRouter
    private var gifImage: Giphy?

    init(interface: GIFDetailsViewProtocol, interactor: GIFDetailsInteractorProtocol?, router: GIFDetailsRouter, gifImage: Giphy) {
        self.view = interface
        self.interactor = interactor
        self.router = router
        self.gifImage = gifImage
    }

    func getImage() -> Giphy? {
        return gifImage
    }
}
