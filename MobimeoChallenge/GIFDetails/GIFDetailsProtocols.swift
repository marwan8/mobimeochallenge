//
//  GIFDetailsProtocols.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 10/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

protocol GIFDetailsPresenterProtocol: class {
    func getImage() -> Giphy?
}

protocol GIFDetailsInteractorProtocol: class {
    var presenter: GIFDetailsPresenterProtocol?  { get set }
}

protocol GIFDetailsViewProtocol: class {
    var presenter: GIFDetailsPresenterProtocol?  { get set }
}
