//
//  GIFDetailsRouter.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 10/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

class GIFDetailsRouter {

    weak var viewController: UIViewController?

    static func createDetailsModule(of image: Giphy) -> UIViewController {
        let view = GIFDetailsViewController()
        let interactor = GIFDetailsInteractor()
        let router = GIFDetailsRouter()
        let presenter = GIFDetailsPresenter(interface: view, interactor: interactor, router: router, gifImage: image)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }
}
