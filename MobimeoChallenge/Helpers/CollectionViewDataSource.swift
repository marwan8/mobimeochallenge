//
//  CollectionViewDataSource.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 06/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

typealias CollectionViewCellConfigureBlock = (_ cell: UICollectionViewCell, _ indexPath: IndexPath?) -> Void

class CollectionViewDataSource: NSObject, UICollectionViewDataSource {

    var itemIdentifier: String
    var numberOfItems: Int?
    var configureCellBlock: CollectionViewCellConfigureBlock

    init(numberOfItems: Int, cellIdentifier: String, collectionView: UICollectionView, configureBlock: @escaping CollectionViewCellConfigureBlock) {
        self.numberOfItems = numberOfItems
        self.itemIdentifier = cellIdentifier
        self.configureCellBlock = configureBlock
        super.init()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItems ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.itemIdentifier, for: indexPath) as UICollectionViewCell
        self.configureCellBlock(cell, indexPath)
        return cell
    }
}
