//
//  SceneDelegate.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 03/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let gifListModule = GIFTrendingListRouter.createModule()
            window.rootViewController = UINavigationController(rootViewController: gifListModule)
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}

