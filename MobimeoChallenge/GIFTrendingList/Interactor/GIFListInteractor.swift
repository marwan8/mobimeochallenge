//
//  GIFListInteractor.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

class GIFListInteractor: GIFListInteractorProtocol {
    var presenter: GIFListPresenterProtocol?
    private let dataProvider: GIFTrendingListDataProvider?

    init(dataProvider: GIFTrendingListDataProvider) {
        self.dataProvider = dataProvider
    }

    func loadTrendingGIFs() {
        dataProvider?.loadItemList { [weak self] (result, error) in
            if let itemList = result as? GiphyList {
                let gifItems = itemList.data
                self?.presenter?.refreshListWithGIFs(gifList: gifItems ?? [])
            } else if let serviceError = error {
                self?.presenter?.handleError(error: serviceError)
            }
        }
    }
}
