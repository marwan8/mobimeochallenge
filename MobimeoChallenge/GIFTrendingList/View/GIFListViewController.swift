//
//  GIFListViewController.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit
import SDWebImageFLPlugin

class GIFListViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    var presenter: GIFListPresenterProtocol?
    private let flowLayout = UICollectionViewFlowLayout()
    private var collectionView: UICollectionView?
    private var dataSource: UICollectionViewDataSource?
    private let activityView =  UIActivityIndicatorView.init(style: .large)

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        layoutUI()
        loadItemsData()
    }

    private func setupUI() {
        setupCollectionView()
        view.addSubview(activityView)
    }

    private func setupCollectionView() {

        flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        flowLayout.scrollDirection = .horizontal

        collectionView = UICollectionView(frame: view.frame, collectionViewLayout: flowLayout)

        collectionView?.register(GIFCollectionViewCell.nib, forCellWithReuseIdentifier: GIFCollectionViewCell.identifier)
        view.addSubview(collectionView ?? UICollectionView.init())
        collectionView?.delegate = self
    }

    private func setupCollectionViewDateSource() {
        dataSource = CollectionViewDataSource.init(numberOfItems: presenter?.itemCount ?? 0, cellIdentifier: GIFCollectionViewCell.identifier, collectionView: collectionView ?? UICollectionView(), configureBlock: { (cell, indexPath) in

            if let gifCell = cell as? GIFCollectionViewCell {
                if let gify = self.presenter?.item(at: indexPath?.row ?? 0) {
                    gifCell.gifImageView?.image = nil
                    gifCell.gifImageView?.sd_setImage(with: URL(string: gify.images?.downSizedImage?.url ?? ""))
                }
            }
        })
        collectionView?.dataSource = dataSource
        collectionView?.reloadData()
        collectionView?.isHidden = false
    }

    private func layoutUI() {
        var frame = self.view.bounds
        frame.size.height -= 100
        collectionView?.frame = frame
        collectionView?.center = CGPoint(x: view.frame.size.width  / 2,
                                         y: view.frame.size.height / 2)
        view.backgroundColor = .white
        collectionView?.backgroundColor = .white
        activityView.center = self.view.center
    }

    private func loadItemsData() {
        collectionView?.isHidden = true
        collectionView?.dataSource = nil
        activityView.startAnimating()
        self.presenter?.loadTrendingGIFs()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = collectionView.bounds
        let screenWidth = screenSize.width
        let cellSquareSize: CGFloat = screenWidth / 4.0
        return CGSize.init(width: cellSquareSize, height: cellSquareSize)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 35.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.showDetails(of: indexPath.row)
    }
}

extension GIFListViewController: GIFListViewProtocol {
    func showError(errorMessage: String) {
        activityView.stopAnimating()
        showAlert(errorMessage)
    }

    func gifItemListLoaded() {
        activityView.stopAnimating()
        setupCollectionViewDateSource()
    }
}
