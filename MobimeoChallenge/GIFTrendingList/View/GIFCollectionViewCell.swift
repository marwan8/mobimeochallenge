//
//  GIFCollectionViewCell.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 07/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit
import SDWebImageFLPlugin

class GIFCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var gifImageView: FLAnimatedImageView?
}
