//
//  GIFTrendingListRouter.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

class GIFTrendingListRouter {

    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        let view = GIFListViewController(nibName: nil, bundle: nil)
        let interactor = GIFListInteractor(dataProvider: GIFTrendingListDataProvider.shared)
        let router = GIFTrendingListRouter()
        let presenter = GIFListPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }

    func showDetails(of image: Giphy) {
        guard let sourceView = viewController else { return }
        let detailsViewController = GIFDetailsRouter.createDetailsModule(of: image)
        sourceView.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}
