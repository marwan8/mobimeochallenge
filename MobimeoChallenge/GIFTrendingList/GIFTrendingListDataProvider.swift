//
//  GIFTrendingListDataProvider.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

class GIFTrendingListDataProvider {
    static let shared = GIFTrendingListDataProvider()
    private let gifListAPI = GIFTrendingListAPI()

    func loadItemList(completion:  @escaping RequestCompletion) {
        gifListAPI.loadItemsList(completionHandler: completion)
    }
}

