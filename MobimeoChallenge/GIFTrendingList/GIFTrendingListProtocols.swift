//
//  GIFTrendingListProtocols.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

protocol GIFListPresenterProtocol: class {
    var itemCount: Int { get }
    func loadTrendingGIFs()
    func refreshListWithGIFs(gifList: [Giphy])
    func handleError(error: Error)
    func item(at index: Int) -> Giphy?
    func showDetails(of index: Int)
}

protocol GIFListInteractorProtocol: class {
    var presenter: GIFListPresenterProtocol?  { get set }
    func loadTrendingGIFs()
}

protocol GIFListViewProtocol: class {
    var presenter: GIFListPresenterProtocol?  { get set }
    func showError(errorMessage: String)
    func gifItemListLoaded()
}

