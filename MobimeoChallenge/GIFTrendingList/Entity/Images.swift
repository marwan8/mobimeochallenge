//
//  Images.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

struct Images: Codable {
    let original: OriginalGIF?
    let downSizedImage: OriginalGIF?

    private enum CodingKeys: String, CodingKey {
        case original
        case downSizedImage = "fixed_height_downsampled"
    }
}
