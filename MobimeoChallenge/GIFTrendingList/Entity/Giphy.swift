//
//  Giphy.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

struct Giphy: Codable {
    let images: Images?
    let title: String?
    let rating: String?
}
