//
//  GIFListPresenter.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

class GIFListPresenter: GIFListPresenterProtocol {
    weak private var view: GIFListViewProtocol?
    var interactor: GIFListInteractorProtocol?
    private let router: GIFTrendingListRouter
    private(set) var gifList: [Giphy]? {
        didSet {
            guard let items = gifList, !items.isEmpty else {
                view?.showError(errorMessage: &&"noItemMessage")
                return
            }
            view?.gifItemListLoaded()
        }
    }

    init(interface: GIFListViewProtocol, interactor: GIFListInteractorProtocol?, router: GIFTrendingListRouter) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    var itemCount: Int {
        return self.gifList?.count ?? 0
    }

    func loadTrendingGIFs() {
        interactor?.loadTrendingGIFs()
    }

    func refreshListWithGIFs(gifList: [Giphy]) {
        self.gifList = gifList
    }

    func handleError(error: Error) {
        view?.showError(errorMessage: error.localizedDescription)
    }

    func item(at index: Int) -> Giphy? {
        return gifList?[index]
    }

    func showDetails(of index: Int) {
        if let item = self.item(at: index) {
            router.showDetails(of: item)
        }
    }
}
