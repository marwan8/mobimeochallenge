//
//  GIFTrendingListAPI.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 05/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import Foundation

class GIFTrendingListAPI {
    enum APIRouter: Requestable {

        case loadItemsList

        var path: String {
            return "trending"
        }

        var parameters: Parameters? {
            return ["api_key": Constants.apiKey,
                    "limit": "25"]
        }
    }
}

extension GIFTrendingListAPI {

    func loadItemsList(completionHandler: @escaping RequestCompletion) {
        var completion: (Result<GiphyList, Error>) -> Void
        completion = { result in
            switch result {
            case .success(let response):
                completionHandler(response, nil)
            case .failure(let error):
                completionHandler(nil, error as NSError)
            }
        }
        APIRouter.loadItemsList.request(completion: completion)
    }
}
