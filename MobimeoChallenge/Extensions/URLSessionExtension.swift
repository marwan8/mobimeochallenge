//
//  URLSessionExtension.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 03/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import Foundation.NSURLSession

extension URLSession {
    func dataLoadingTask(with request: URLRequest, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionDataTask {
        return dataTask(with: request) { (data, response, error) in
            if let error = error {
                result(.failure(error))
                return
            }
            guard let response = response, let data = data else {
                let error = NSError(domain: "error", code: 0, userInfo: nil)
                result(.failure(error))
                return
            }
            result(.success((response, data)))
        }
    }
}
