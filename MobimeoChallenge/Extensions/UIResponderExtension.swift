//
//  UIResponderExtension.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 07/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit.UIResponder

protocol Identifiable {
    static var identifier: String { get }
}

extension UIResponder: Identifiable {
    static var identifier: String {
        return "\(self)"
    }
}
