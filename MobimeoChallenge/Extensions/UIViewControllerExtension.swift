//
//  UIViewControllerExtension.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 13/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit.UIAlertController

extension UIViewController {

    func showAlert(_ title: String, message: String = "", okBlock: (() -> Void)? = nil, cancel: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: &&"OK", style: .default) { _ in
            okBlock?()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
