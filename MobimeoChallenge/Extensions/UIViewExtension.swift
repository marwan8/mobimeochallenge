//
//  UIViewExtension.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 07/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit.UIView

//localization
prefix operator &&

prefix func && (string: String?) -> String {
    guard let string = string else { return "" }
    return NSLocalizedString(string, comment: "")
}

extension UIView {

    // get nib from bundle
    class var nib: UINib {
        return UINib(nibName: nibName, bundle: nil)
    }

    // get nib name
    class var nibName: String {
        return identifier
    }
}
