//
//  GIFImageViewExtension.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 08/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

//import UIKit
//
//extension GIFImageView {
//
//    func downloadedGIF(from link: String) {
//        guard let url = URL(string: link) else { return }
//        downloadedGIF(from: url)
//    }
//
//    private func downloadedGIF(from url: URL) {
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            guard
//                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
//                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
//                let data = data, error == nil
//                else { return }
//
//            DispatchQueue.main.async {
//                //self.animate(withGIFData: data)
//            }
//        }.resume()
//    }
//}
