//
//  AppDelegate.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 03/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

