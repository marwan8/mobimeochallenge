//
//  HTTPMethod.swift
//  MobimeoChallenge
//
//  Created by Marwan Ayman on 03/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case delete  = "DELETE"
}
