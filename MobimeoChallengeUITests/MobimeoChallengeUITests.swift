//
//  MobimeoChallengeUITests.swift
//  MobimeoChallengeUITests
//
//  Created by Marwan Ayman on 13/03/2020.
//  Copyright © 2020 Marwan Ayman. All rights reserved.
//

import XCTest

class MobimeoChallengeUITests: XCTestCase {

    let app = XCUIApplication()

    override func setUp() {
        continueAfterFailure = false
        app.launch()
        app.tap()
    }

    func testNotEmptyList() {
        let count = app.collectionViews.children(matching: .any).count
        XCTAssertNotEqual(count, 0)
    }

    func testCollectionViewTap() {
        let firstRowElement = app.collectionViews.cells.allElementsBoundByIndex[0]
        let exist = firstRowElement.waitForExistence(timeout: 2)

        if exist {
            XCTAssertTrue(firstRowElement.exists)
            firstRowElement.tap()
        }
    }
}
