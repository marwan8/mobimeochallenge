# MobimeoChallenge App

This task consist of a simple navigation-based animated gif browser and gif detail view using  VIPER Architecture.

## About VIPER

### Viper Components

Each VIPER module consists of the following parts:

- View (View, ViewController) 
- Interactor (Business Logic, Use Cases) 
- Presenter (Prepare Business logic for presentation in the View)
- Entity (Model) 
- Router (Assemble each module and Take control of Routing)

VIPER architecture allows for a clearer separation of concerns and separates the viewController from handling most of the responsibility in an app. Concerns are separated into modules for each use case.
Each module has a clear layer of routing logic, presentation logic and business logic
												
## About the task App
The app consist of a simple navigation-based animated gif browser and gif detail view.

There are in total 1 VIPER module.

The UI appearance of this app is currently under construction 😂

## Run
Please run the project using Xcode 11, if you face any pod error please re-install
The porject support Swift 5 
Please note that I am using Swiftlint for appling code linting

## Unit Testing
One advantage of VIPER is that it makes unit testing so much easier!

So, I have add some examples of unit testing as well as UI Testing

## TODO:
1. Implement more real-life like UI
2. Add animations
3. Add more test cases 
4. Implement my own code that handles gif rendering.
5. Support an offline mode, which stores the gifs on disk, so you can enjoy all fetched gifs while there is no network connection
available.
6. Enhance Error handling 

# Contact:
marwan.ayman8@gmail.com

